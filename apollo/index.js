import { ApolloServer } from "@apollo/server";
import { startStandaloneServer } from "@apollo/server/standalone";

import { PrismaClient } from "@prisma/client";

import bcrypt from "bcrypt";

const saltRounds = 10;
const prisma = new PrismaClient();

// data
import db from "./_db.js";

// types
import { typeDefs } from "./schema.js";

import Users from "./classes/Users.js";

import { v4 as uuidv4 } from "uuid";
import { parse } from "graphql";

// resolvers
const resolvers = {
  UserOrMessage: {
    __resolveType(obj) {
      if (obj.login && obj.hash) {
        return "User";
      }
      if (obj.header && obj.content) {
        return "Message";
      }
      return null;
    },
  },
  PostOrMessage: {
    __solverType(obj) {
      if (obj.title && obj.createdAt && obj.content) {
        return "Post";
      }
      if (obj.header && obj.content) {
        return "Message";
      }
      return null;
    },
  },
  CommentOrMessage: {
    __resolveType(obj) {
      if (obj.createdAt && obj.content) {
        return "Comment";
      }
      if (obj.header && obj.content) {
        return "Message";
      }
      return null;
    },
  },
  UserPostOrMessage: {
    __resolveType(obj) {
      if (obj.userId && obj.postId) {
        return "UserPost";
      }
      if (obj.header && obj.content) {
        return "Message";
      }
      return null;
    },
  },
  UserCommentOrMessage: {
    __resolveType(obj) {
      if (obj.userId && obj.commentId) {
        return "UserComment";
      }
      if (obj.header && obj.content) {
        return "Message";
      }
      return null;
    },
  },
  Query: {
    users() {
      return prisma.user.findMany();
    },
    async user(_, args) {
      const user = await prisma.user.findUnique({
        where: {
          login: args.login,
        },
      });

      if (user) {
        console.log("Znaleziono użytkownika:", user);
        const result = await bcrypt.compare(args.password, user.hash);
        if (result) {
          const uuid = uuidv4();
          Users.addUser(uuid, user);
          return { ...user, uuid };
        }
        return { header: "Error", content: "Wrong creditentials" };
      } else {
        return { header: "Error", content: "User does not exist" };
      }
    },
    logout(_, args) {
      Users.removeUser(_, args.uuid);
      return { header: "Message", content: "User logged out" };
    },
    async posts(_, args) {
      try {
        const user = Users.getUser(args.uuid);
        const month = args.month > 9 ? args.month : "0" + args.month;
        let nextMonth = args.month + 1 > 9 ? args.month + 1 : "0" + (args.month + 1);
        let nextYear = args.year;
        if (nextMonth === 13) {
          nextMonth = "01";
          nextYear++;
        }
    
        let postsInMonth;
    
        if (!user) {
          postsInMonth = await prisma.post.findMany({
            where: {
              createdAt: {
                gte: new Date(`${args.year}-${month}-01T00:00:00.000Z`),
                lt: new Date(`${nextYear}-${nextMonth}-01T00:00:00.000Z`),
              },
            },
            include: {
              User: true,
            },
          });
        } else {
          postsInMonth = await prisma.post.findMany({
            where: {
              createdAt: {
                gte: new Date(`${args.year}-${month}-01T00:00:00.000Z`),
                lt: new Date(`${nextYear}-${nextMonth}-01T00:00:00.000Z`),
              },
              // userPost: {
              //   some: {
              //     user: {
              //       id: user.id,
              //     },
              //   },
              // },
            },
            include: {
              User: true,
              userPost: {
                where: {
                  user: {
                    id: user.id,
                  },
                },
                select: {
                  mark: true,
                },
              },
            },
          });
        }
    
        const enrichedPosts = await Promise.all(
          postsInMonth.map(async (post) => {
            const POSITIVE = await prisma.userPost.count({
              where: {
                postId: post.id,
                mark: 'POSIIVE',
              },
            });
    
            const NEGATIVE = await prisma.userPost.count({
              where: {
                postId: post.id,
                mark: 'NEGATIVE',
              },
            });
    
            return { ...post, POSITIVE, NEGATIVE };
          })
        );
    
        return enrichedPosts;
      } catch (error) {
        // Handle errors appropriately
        console.error("Error fetching posts:", error);
        throw new Error("Failed to fetch posts");
      }
    }
,    

    async findEarliestPost() {
      const earliestPost = await prisma.post.findFirst({
        orderBy: { createdAt: "asc" },
      });

      return earliestPost;
    },
    async findCommentsToPost(_, args) {
      let user;
      if(args.uuid) {
          user = Users.getUser(args.uuid);
      }

      let comments;
      if(!user) {
        comments = await prisma.comment.findMany({
          where: {
            postId: parseInt(args.id),
          },
          include: {
            user: true,
          },
          orderBy: {
            createdAt: "asc",
          },
        });
      } else {
        comments = await prisma.comment.findMany({
          where: {
            postId: parseInt(args.id),
          },
          include: {
            user: true,
            userComment: true
          },
          orderBy: {
            createdAt: "asc"
          }
        });
      }
        
      const commentsWithCounts = comments.map(async comment => {
        const POSIIVE =  await prisma.userComment.count({
          where: {
            commentId: comment.id,
            mark: 'POSIIVE' // Dodajemy warunek mark równy 'POSITIVE'
          }
        });
        const NEGATIVE =  await prisma.userComment.count({
          where: {
            commentId: comment.id,
            mark: 'NEGATIVE' // Dodajemy warunek mark równy 'POSITIVE'
          }
        });
        return {...comment, POSIIVE, NEGATIVE}
      }
          
      )
      return Promise.all(commentsWithCounts);
    },
    async findCommentsToComment(_, args) {
      let user;
      if(args.uuid) {
          user = Users.getUser(args.uuid);
      }

      let comments;
      if(!user) {
        comments = await prisma.comment.findMany({
          where: {
            commentId: parseInt(args.id),
          },
          include: {
            user: true,
          },
          orderBy: {
            createdAt: "asc",
          },
        });
      } else {
        comments = await prisma.comment.findMany({
          where: {
            commentId: parseInt(args.id),
            userId: user.id // zmienna args.userId
          },
          include: {
            user: true,
            userComment: true
          },
          orderBy: {
            createdAt: "asc"
          }
        });
      }
        
      const commentsWithCounts = comments.map(async comment => {
        const POSIIVE =  await prisma.userComment.count({
          where: {
            commentId: comment.id,
            mark: 'POSIIVE' // Dodajemy warunek mark równy 'POSITIVE'
          }
        });
        const NEGATIVE =  await prisma.userComment.count({
          where: {
            commentId: comment.id,
            mark: 'NEGATIVE' // Dodajemy warunek mark równy 'POSITIVE'
          }
        });
        return {...comment, POSIIVE, NEGATIVE}
      }
          
      )
      return Promise.all(commentsWithCounts);
    },
  },

  Mutation: {
    async addUser(_, args) {
      const hashedPassword = await new Promise((resolve, reject) => {
        bcrypt.hash(args.password, saltRounds, function (err, hash) {
          if (err) reject(err);
          resolve(hash);
        });
      });

      let user = { login: args.login, hash: hashedPassword };
      await prisma.user.create({ data: user });
      return user;
    },
    async addPost(_, args) {
      const user = Users.getUser(args.uuid);
      if (typeof user === "undefined") {
        return {
          __typename: "Message",
          header: "Error",
          content: "User has not been found",
        };
      }
      const { id } = user;

      const createdPost = await prisma.post.create({
        data: {
          title: args.title,
          content: args.content,
          userId: id,
        },
      });

      return { ...createdPost, __typename: "Post" };
    },

    async addComment(_, args) {
      const user = Users.getUser(args.uuid);
      if (typeof user === "undefined") {
        return {
          __typename: "Message",
          header: "Error",
          content: "User has not been found",
        };
      }
      const { id } = user;
      let createdComment;
      if (args.postId) {
        createdComment = await prisma.comment.create({
          data: {
            content: args.content,
            userId: id,
            postId: parseInt(args.postId),
          },
        });
      } else {
        createdComment = await prisma.comment.create({
          data: {
            content: args.content,
            userId: id,
            commentId: parseInt(args.commentId),
          },
        });
      }
      return createdComment;
    },
    async addUserPost(_, args) {
      const user = Users.getUser(args.uuid);
      if (typeof user === "undefined") {
        return {
          __typename: "Message",
          header: "Error",
          content: "User has not been found",
        };
      }
      const { id } = user;

      const userPostExisting = await prisma.userPost.findFirst({
        where: {
          userId: id,
          postId: parseInt(args.postId),
        },
      });
      let userPost;
      if (userPostExisting === null) {
        userPost = await prisma.userPost.create({
          data: {
            userId: id,
            postId: parseInt(args.postId),
            mark: args.mark,
          },
        });
      } else {
        // userPostExisting.mark = args.mark;
        // const userPost = await prisma.userPost.save(userPostExisting);
        userPost = await prisma.userPost.update({
          where: {
            id: userPostExisting.id,
          },
          data: {
            mark: args.mark,
          },
        });
      }

      return userPost;
    },
    async addUserComment(_, args) {
      const user = Users.getUser(args.uuid);
      if (typeof user === "undefined") {
        return {
          __typename: "Message",
          header: "Error",
          content: "User has not been found",
        };
      }
      const { id } = user;

      const userCommentExisting = await prisma.userComment.findFirst({
        where: {
          userId: id,
          commentId: parseInt(args.commentId),
        },
      });
      let userComment;
      if (userCommentExisting === null) {
        userComment = await prisma.userComment.create({
          data: {
            userId: id,
            commentId: parseInt(args.commentId),
            mark: args.mark,
          },
        });
      } else {
        // userPostExisting.mark = args.mark;
        // const userPost = await prisma.userPost.save(userPostExisting);
        userComment = await prisma.userComment.update({
          where: {
            id: userCommentExisting.id,
          },
          data: {
            mark: args.mark,
          },
        });
      }

      return userComment;
    },
  },
};

// server setup
const server = new ApolloServer({
  typeDefs,
  resolvers,
  cors: {
    origin: true,
    credentials: true, // true if you need cookies/authentication
    methods: ["GET", "POST", "OPTIONS"],
  },
});

const { url } = await startStandaloneServer(server, {
  listen: { port: 4000 },
});

console.log(`Server ready at: ${url}`);
