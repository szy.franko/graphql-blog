/*
  Warnings:

  - The values [POSIIVE] on the enum `UserComment_mark` will be removed. If these variants are still used in the database, this will fail.
  - The values [POSIIVE] on the enum `UserComment_mark` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterTable
ALTER TABLE `usercomment` MODIFY `mark` ENUM('POSITIVE', 'NEGATIVE', 'NEUTRAL') NOT NULL;

-- AlterTable
ALTER TABLE `userpost` MODIFY `mark` ENUM('POSITIVE', 'NEGATIVE', 'NEUTRAL') NOT NULL;
