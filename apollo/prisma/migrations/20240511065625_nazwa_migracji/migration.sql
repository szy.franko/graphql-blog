/*
  Warnings:

  - You are about to alter the column `content` on the `comment` table. The data in that column could be lost. The data in that column will be cast from `VarChar(5000)` to `VarChar(1000)`.

*/
-- AlterTable
ALTER TABLE `comment` MODIFY `content` VARCHAR(1000) NULL;

-- AlterTable
ALTER TABLE `post` MODIFY `content` VARCHAR(5000) NULL;
