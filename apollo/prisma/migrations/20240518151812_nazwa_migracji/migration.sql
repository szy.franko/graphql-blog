/*
  Warnings:

  - The values [POSITIVE] on the enum `UserComment_mark` will be removed. If these variants are still used in the database, this will fail.
  - The values [POSITIVE] on the enum `UserComment_mark` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterTable
ALTER TABLE `usercomment` MODIFY `mark` ENUM('POSIIVE', 'NEGATIVE', 'NEUTRAL') NOT NULL;

-- AlterTable
ALTER TABLE `userpost` MODIFY `mark` ENUM('POSIIVE', 'NEGATIVE', 'NEUTRAL') NOT NULL;
