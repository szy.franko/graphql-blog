export const typeDefs = `#graphql
  
  enum Mark {
    POSIIVE
    NEGATIVE
    NEUTRAL
  }
  type User {
    id: ID!
    login: String!
    hash: String
    uuid: String
    isAdmin: Boolean
  }
  type Post {
    id: ID!
    title: String!
    createdAt: String!
    content: String!
  }
  type Comment {
    id: ID!
    createdAt: String!
    content: String!
    postId: ID
    POSIIVE: Int
    NEGATIVE: Int
    commentId: ID
    user: User
  }
  type UserPost {
    mark:Mark!
  }
  type UserComment {
    mark:Mark!
  }
  type PostWithUserWithUserPost {
    id: ID!
    title: String!
    createdAt: String!
    content: String!
    User: User!
    POSITIVE: Int!
    NEGATIVE: Int!
    userPost: [UserPost]
  }
  type CommentWithUserWithUserComment {
    id: ID!
    createdAt: String!
    content: String!
    user: User!
    POSIIVE: Int
    NEGATIVE: Int
    userComment: [UserComment]
  }

  type Message {
    header: String!
    content: String!
  }
  type UserPost {
    id: ID!
    userId: ID!
    user: User
    postId: ID!
    post: Post
    mark: Mark
  }
  type UserComment {
    id: ID!
    userId: ID!
    user: User
    commentId: ID!
    comment: Comment
    mark: Mark
  }

  union UserOrMessage = User | Message
  union PostOrMessage = Post | Message
  union CommentOrMessage = Comment | Message
  union UserPostOrMessage = UserPost | Message
  union UserCommentOrMessage = UserComment | Message

  type Query {
    users: [User]
    user(login: String, password: String): UserOrMessage
    logout(uuid: String): Message
    posts(month:Int, year: Int, uuid: String): [PostWithUserWithUserPost!]
    findEarliestPost: Post!
    findCommentsToPost(id:ID!, uuid: String): [CommentWithUserWithUserComment!]
    findCommentsToComment(id:ID!, uuid:String): [CommentWithUserWithUserComment!]
  }
  type Mutation {
    addUser(login: String, password: String): UserOrMessage
    addPost(title: String, content: String, uuid:String):PostOrMessage
    addComment(content: String, postId: ID, commentId: ID, uuid:String): CommentOrMessage
    addUserPost(postId: ID, mark:Mark, uuid:String):UserPostOrMessage
    addUserComment(commentId: ID, mark:Mark, uuid:String):UserCommentOrMessage
  }
`;
