export default class Users {
    // Prywatna właściwość przechowująca obiekt users
    static #users = {};
  
    // Metoda publiczna do dodawania użytkownika
    static addUser(userId, userDetails) {
      Users.#users[userId] = userDetails;
    }
  
    // Metoda publiczna do pobierania użytkownika
    static getUser(userId) {
      return Users.#users[userId];
    }
  
    // Metoda publiczna do usuwania użytkownika
    static removeUser(userId) {
      delete Users.#users[userId];
    }

    static getUSers() {
      return Users.#users;
    }
  
    // Statyczna metoda zwracająca instancję singletona
    // static getInstance() {
    //   if (!this.instance) {
    //     this.instance = new UsersSingleton();
    //   }
    //   return this.instance;
    // }
  }