import create, { SetState } from 'zustand';
import { Mark } from '../helpers';


interface User {
  login: string;
  uuid?: string;
  isAdmin?: boolean;
}

interface Post {
  id: string;
  title: string;
  content: string;
  createdAt: string;
  User: User;
  userPost?: {
    mark?: Mark;
  }[];
  POSITIVE: number;
  NEGATIVE: number;
}

interface StoreState {
  count: number;
  user: User | null;
  loginDialog: boolean;
  registerDialog: boolean;
  posts: Post[];
  year: number;
  month: number;
  newPosts: boolean;
  newComments: boolean;
}
  
interface StoreActions {
  increment: () => void;
  decrement: () => void;
  login:(user: User) => void;
  logout: () => void;
  toggleLoginDialog: () => void;
  toggleRegisterDialog: () =>void;
  setPosts: (posts: []) => void;
  setYear: (year: number) =>void;
  setMonth: (month: number) => void;
  setNewPosts: ()=>void;
  markPost: (id: string, mark: Mark) => void;
  setNewComments: ()=>void;
}

const useStore = create<StoreState & StoreActions>((set: SetState<StoreState>) => ({
  count: 0,
  user: null,
  loginDialog: false,
  registerDialog: false,
  posts: [],
  year: (new Date()).getFullYear(),
  month:(new Date()).getMonth(),
  newPosts: false,
  newComments: false, 
  increment: () => set((state) => ({ count: state.count + 1 })),
  decrement: () => set((state) => ({ count: state.count - 1 })),
  login: (user:User) => set((state) => ({ user })),
  logout: ()=>{set((state) => ({ user: null }))},
  toggleLoginDialog: () => set((state) => ({ loginDialog: !state.loginDialog })),
  toggleRegisterDialog: () => set((state) => ({ registerDialog: !state.registerDialog })),
  setPosts: (posts:[])=> set((state)=>({posts})),
  setYear: (year: number)=>set(state=>({year})),
  setMonth: (month: number)=>set(state=>({month})),
  setNewPosts: () => set((state) => ({ newPosts: !state.newPosts })),  
  markPost: (id: string, mark: Mark) => set((state) => ({
    ...state,
    posts: state.posts.map(post => {

      if (post.id === id) {
        return {
          ...post,
          mark: mark
        };
      }
      // W przeciwnym razie zwróć post bez zmian
      return post;
    })
  })),
  setNewComments: () => set((state) => ({ newComments: !state.newComments })),

}));

export default useStore;