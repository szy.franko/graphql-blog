import { useEffect } from "react";
import "./App.css";
import useStore from "./store";
import { ApolloProvider, ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";// import Games from './components/Games';

import { createTheme, ThemeProvider } from '@mui/material/styles';

import Home from "./components/Home";
import Blog from "./components/Blog";
import About from "./components/About";
import Menu from './components/Menu';
import Login from "./components/Login";
import Register from "./components/Register";
import Panel from "./components/Panel";


const httpLink = createHttpLink({
  uri: "http://localhost:4000",
});

const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
});


const theme = createTheme({
  palette: {
    primary: {
      main: '#A05D4D',
    },
    secondary: {
      main: '#255CF5',
    },
  },
});


function App() {
  const { user, registerDialog } = useStore();
  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <Router>
          <div className="App">
            <Menu />
            {user !== null && <Panel />}
            <div>

              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/blog" element={<Blog />} />
                <Route path="/about" element={<About />} />
              </Routes>
              <Login />
              {registerDialog && <Register />}
            </div>
          </div>
        </Router>
      </ThemeProvider>
    </ApolloProvider>
  );
}

export default App;
