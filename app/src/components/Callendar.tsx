import { FC, useState, useEffect } from 'react';
import { useQuery, gql } from "@apollo/client";

import Months from './Months';

interface Props {}

const FIND_EARLIEST_POST_QUERY = gql`
  query FindEarliestPost {
    findEarliestPost {
      createdAt
    }
  }
`;

const Callendar: FC<Props> = () => {
  const [start, setStart] = useState<Date| null>(null);
  const [now, setNow] = useState<Date| null>(null);
  const [years, setYears] = useState<{ [key: number]: number[] }>({});
  const { loading, error, data } = useQuery(FIND_EARLIEST_POST_QUERY);


  const months = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];
  
  useEffect(() => {
    
    if (data && data.findEarliestPost) {
      setStart(start=>new Date(parseInt(data.findEarliestPost.createdAt)));
      setNow(now => new Date(Date.now()));
      
    }
  }, [data]);

  useEffect(()=>{
    if(start && now) {
      const startMonth = start.getMonth();
      const nowMonth = now.getMonth();
      const updatedYears:any = {};
      if(start.getFullYear()===now.getFullYear()) {
        updatedYears[now.getFullYear()]=[]
        // setYears({ [now.getFullYear()]: [] });
        for(let i=startMonth; i<=nowMonth; i++) {
          updatedYears[now.getFullYear()].push(i);
        }
        setYears(years=>{ return {...updatedYears}});
        return;
      }
      for(let i=now.getFullYear(); i>=start.getFullYear(); i--) {
        updatedYears[i] = [];
        if(i===start.getFullYear()) {
          for(let j=start.getMonth()+1;j<=12;j++) {
            updatedYears[start.getFullYear()].push(j-1);
          }
        } else if (i===now.getFullYear()){
          for(let j=0;j<now.getMonth()+1;j++) {
            updatedYears[now.getFullYear()].push(j);
          }
        } else {
          for(let j=1;j<13; j++){
            updatedYears[i].push(j-1);
          }
        }
        
      }
      setYears(years=> {return {...updatedYears}}) 
    } 
  },[start, now])

  return (
    <div id="callendar-wrapper">
      <Months years={years}/>
    </div>
  );
}

export default Callendar;
