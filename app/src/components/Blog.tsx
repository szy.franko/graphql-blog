  import React, { FC, useRef, useState } from "react";
  import { useMutation, gql } from "@apollo/client";
  import { Editor } from "@tinymce/tinymce-react";
  import Button from "@mui/material/Button";
  import TextField from "@mui/material/TextField";

  import Callendar from "./Callendar";
  import useStore from "../store";
  import Post from "./Post";

  const SENDPOST_MUTATION = gql`
    mutation AddPost($title: String, $content: String, $uuid: String) {
      addPost(title: $title, content: $content, uuid: $uuid) {
        ... on Post {
          id
          title
          createdAt
          content
        }
        ... on Message {
          header
          content
        }
      }
    }
  `;

  const Blog: FC = () => {
    const { user, posts, setNewPosts } = useStore();
    const editorRef = useRef<any>(null);

    const [title, setTitle] = useState<string>("");
    const [showEditor, setShowEditor] = useState<boolean>(false);

    const [sendPostMutation, { loading, error, data }] =
      useMutation(SENDPOST_MUTATION);

    const sendPost = async () => {
      const result = await sendPostMutation({
        variables: {
          title,
          content: editorRef.current.getContent(),
          uuid: user?.uuid,
        },
      });
      setTitle("");
      editorRef.current.setContent("");
      setNewPosts();
    };

    return (
      <>
        {user !== null && user.isAdmin === true && (
          <Button
            onClick={() => setShowEditor((prevShowEditor) => !prevShowEditor)}
            style={{
              position: 'absolute',
              right: '20px',
              top: '10px',
              height: '50px'
            }}
          >
            {showEditor ? <p>hide editor</p> : <p>show editor</p>}
          </Button>
        )}
        {user !== null && user.isAdmin === true && showEditor && (
          <TextField
            onChange={(e) => setTitle(e.target.value)}
            value={title}
            id="outlined-basic"
            label="Title"
            variant="outlined"
          />
        )}
        {user !== null && user.isAdmin === true && showEditor && (
          <Editor
            apiKey="czbdz7f9e00bb0btuqubcg5343htm55d9mapykzezxrza2x6"
            onInit={(_evt, editor) => (editorRef.current = editor)}
            initialValue="<p>This is the initial content of the editor.</p>"
            init={{
              height: 500,
              menubar: false,
              plugins: [
                "advlist",
                "autolink",
                "lists",
                "link",
                "image",
                "charmap",
                "preview",
                "anchor",
                "searchreplace",
                "visualblocks",
                "code",
                "fullscreen",
                "insertdatetime",
                "media",
                "table",
                "code",
                "help",
                "wordcount",
              ],
              toolbar:
                "undo redo | blocks | " +
                "bold italic forecolor | alignleft aligncenter " +
                "alignright alignjustify | bullist numlist outdent indent | " +
                "removeformat | help",
              content_style:
                "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
            }}
          />
        )}
        {user !== null && user.isAdmin === true && showEditor && (
          <Button onClick={sendPost} style={{ margin: "10px" }} color="success">
            Add post
          </Button>
        )}

        <Callendar />
        {posts.map((post, index) => (
          <Post
            key={index}
            id={parseInt(post.id)}
            title={post.title}
            content={post.content}
            createdAt={post.createdAt}
            author={post.User.login}
            mark={post.userPost && post.userPost[0] ? post.userPost[0].mark : undefined}
            POSITIVE={post.POSITIVE}
            NEGATIVE={post.NEGATIVE}
          />
        ))}
      </>
    );
  };

  export default Blog;
