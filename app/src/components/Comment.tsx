import React, { useState, useEffect, useCallback } from "react";
import { useMutation, useLazyQuery, gql } from "@apollo/client";
import Button from '@mui/material/Button';
import TextField from "@mui/material/TextField";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import AddIcon from '@mui/icons-material/Add';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import useStore from "../store";
import { Mark } from '../helpers';
import Comment from './Comment';

interface Props {
    id: number;
    content: String;
    createdAt: string;
    login: string;
    mark?: Mark;
    POSIIVE: number;
    NEGATIVE: number;
    callback:()=>void
}

interface User {
    login: string;
}

interface Comment {
    content: string;
    createdAt: string;
    id: number;
    user: User;
    POSIIVE: number;
    NEGATIVE: number;
}

const SENDCOMMENT_MUTATION = gql`
  mutation AddComment($content: String, $commentId: ID, $uuid: String) {
    addComment(content: $content, commentId: $commentId, uuid: $uuid) {
      ... on Comment {
        id
        createdAt
        content
      }
      ... on Message {
        header
        content
      }
    }
  }
`;

const FIND_COMMENTS_TO_COMMENT_QUERY = gql`
query findCommentsToComment($id: ID!, $uuid:String) {
  findCommentsToComment(id: $id, uuid: $uuid) {
    content
    createdAt
    id
    POSIIVE
    NEGATIVE
    userComment {
        mark
    }
    user {
      login
    }
  }
}`;

const SENDUSERCOMMENT_MUTATION = gql`
  mutation AddUserComment($commentId: ID, $mark: Mark, $uuid: String) {
    addUserComment(commentId: $commentId, mark: $mark, uuid: $uuid) {
      ... on UserComment {
        id
        commentId
        mark
      }
      ... on Message {
        header
        content
      }
    }
  }
`;

const CommentComponent: React.FC<Props> = ({ id, content, createdAt, login, mark, POSIIVE, NEGATIVE, callback }: Props) => {
    const { user, setNewComments, posts } = useStore();
    const [commentContent, setCommentContent] = useState<string>('');
    const [comments, setComments] = useState<Comment[]>([]);
    const [showComments, setShowComments] = useState<boolean>(false);
    const [commentMark, setCommentMark] = useState<Mark | undefined>(mark);


    const [sendCommentMutation] = useMutation(SENDCOMMENT_MUTATION);
    const [findCommentsToComment] = useLazyQuery(FIND_COMMENTS_TO_COMMENT_QUERY, { fetchPolicy: 'network-only' });
    const [sendUserCommentMutation] = useMutation(SENDUSERCOMMENT_MUTATION);

    const getComments = async ()=>{
        const resultComments = await findCommentsToComment({ variables: { id, uuid: user?.uuid } });
        setComments(resultComments.data.findCommentsToComment);
    }
    const expandComments = () => {
        setShowComments(true);
        getComments();
    };

    const sendComment = useCallback(async () => {
        await sendCommentMutation({
            variables: {
                content: commentContent,
                uuid: user?.uuid,
                commentId: id
            },
        });
        const resultComments = await findCommentsToComment({ variables: { id } });
        setComments(resultComments.data.findCommentsToComment);
        setCommentContent('');
    }, [commentContent, sendCommentMutation, findCommentsToComment, id, user?.uuid]);

    const sendUserComment = async (mark: Mark) => {
        await sendUserCommentMutation({
            variables: {
                commentId: id, 
                mark, 
                uuid: user?.uuid
            }
        });
        setCommentMark(mark);
        setNewComments();
        callback();
    };
    useEffect(()=>{
      setCommentMark(mark);
    },[mark]);
    return (
        <div className="comment">
            <div className="comment-content">
                {content}
            </div>
            <div>
                {login}, {(new Date(parseInt(createdAt))).toLocaleDateString()}
            </div>
            {user && <div style={{transform: 'translate(47px,10px)'}}>
                <ThumbUpIcon style={{width: '20px'}} className={commentMark === Mark.POSIIVE ? 'positive-mark' : ''} onClick={() => { sendUserComment(Mark.POSIIVE); }} />
                <ThumbDownIcon style={{width: '20px'}} className={commentMark === Mark.NEGATIVE ? 'negative-mark' : ''} onClick={() => { sendUserComment(Mark.NEGATIVE); }} />
            </div>}
            <div className="marks-number">
        <p className="positive-mark">{ POSIIVE}</p> <p className="negative-mark">{ NEGATIVE }</p>
        </div>
            {user !== null && !showComments && <Button onClick={expandComments}><ArrowDropDownIcon /></Button>}
            {user !== null && showComments && <Button onClick={() => { setShowComments(false); }}><ArrowDropUpIcon /></Button>}

            {showComments && <List className="comments-list">
                {comments.map((el, key) => <ListItem key={key} disablePadding>
                    <Comment
                        id={el.id}
                        content={el.content}
                        createdAt={el.createdAt}
                        login={el.user.login}
                        POSIIVE={el.POSIIVE}
                        NEGATIVE={el.NEGATIVE}
                        callback={getComments} />
                </ListItem>)}
            </List>}

            {user !== null && showComments && <>
                <TextField
                    onChange={(e) => {
                        setCommentContent(e.target.value);
                    }}
                    value={commentContent}
                    multiline
                    id="outlined-basic"
                    label="Comment"
                    variant="outlined"
                />
                <Button onClick={sendComment}><AddIcon /></Button>
            </>}
        </div>
    );
};

export default CommentComponent;
