import { useState } from 'react';
import { useLazyQuery, gql } from "@apollo/client";

import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';

import useStore from "../store";

const LOGIN_QUERY = gql`
 query FindUser($login: String, $password: String) {
  user(login: $login, password: $password) {
    
    ... on User {
      login
      isAdmin
      uuid
    }
    ... on Message {
      header
      content
    }
  }
}
`;

export interface SimpleDialogProps {
    open: boolean;
    onClose: () => void;
}

function SimpleDialog(props: SimpleDialogProps) {
    const { onClose, open } = props;
    const { login, setNewPosts } = useStore();
    const [loginText, setLoginText] = useState<string>('');
    const [passwordText, setPasswordText] = useState<string>('');
    const [wrongCreditentials, setWrongCreditentials] = useState<boolean>(false);

    const [loginQuery, { loading, error, data }] = useLazyQuery(LOGIN_QUERY);

    const onLogin = async () => {
        const result = await loginQuery({ variables: { login: loginText, password: passwordText } });
        
        if(result.data?.user?.content==='User does not exist' || result.data?.user?.content==='Wrong creditentials') {
            setWrongCreditentials(true);
        }

        if(typeof result.data?.user?.login!=='undefined') {
            login(result.data.user);
            setNewPosts();
            onClose();
        } else {

        }
        
    };

    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>Login</DialogTitle>
            <Box
                component="form"
                sx={{
                    '& > :not(style)': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="off"
                style={{ display: 'flex', flexDirection: 'column', padding: '20px' }}
            >
                <TextField onChange={(e) => { setLoginText(e.target.value) }} id="standard-basic" label="login" variant="standard" />
                <TextField
                    onChange={(e) => { setPasswordText(e.target.value) }}
                    id="outlined-password-input"
                    variant="standard"
                    label="password"
                    type="password"
                    autoComplete="current-password"
                />
            </Box>

            <ButtonGroup style={{ display: 'flex', justifyContent: 'space-around' }} variant="text" aria-label="Basic button group">
                <Button onClick={onLogin} style={{ margin: '10px' }} color="success">Login</Button>
                <Button onClick={onClose} style={{ margin: '10px' }} color="secondary">Close</Button>
            </ButtonGroup>
           { wrongCreditentials && <Stack sx={{ width: '100%' }} spacing={2}>
                <Alert severity="error">Wrong credentials</Alert>
            </Stack> }
        </Dialog>
    );
}

export default () => {
    const { loginDialog, toggleLoginDialog } = useStore();


    return (
        <div>
            <SimpleDialog
                open={loginDialog}
                onClose={toggleLoginDialog}
            />
        </div>
    );
}
