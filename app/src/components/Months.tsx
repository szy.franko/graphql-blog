import React, { useEffect } from 'react';
import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import MuiAccordionSummary, {
    AccordionSummaryProps,
} from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import { useLazyQuery, gql } from "@apollo/client";

import useStore from '../store';

const Accordion = styled((props: AccordionProps) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
        borderBottom: 0,
    },
    '&::before': {
        display: 'none',
    },
}));

const AccordionSummary = styled((props: AccordionSummaryProps) => (
    <MuiAccordionSummary
        expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
        {...props}
    />
))(({ theme }) => ({
    backgroundColor:
        theme.palette.mode === 'dark'
            ? 'rgba(255, 255, 255, .05)'
            : 'rgba(0, 0, 0, .03)',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
        marginLeft: theme.spacing(1),
    },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
}));


interface MonthsProps {
    years: object; // Obiekt reprezentujący lata i ich miesiące
}

const FIND_POSTS_QUERY = gql`
query FindPosts($month: Int, $year: Int) {
  posts(month: $month, year: $year) {
    id
    title
    content
    createdAt
    POSITIVE
    NEGATIVE
    User {
      login
    }
    
  }
}`

const FIND_POSTS_FOR_LOGGED_QUERY = gql`
query FindPosts($month: Int, $year: Int, $uuid: String) {
  posts(month: $month, year: $year, uuid: $uuid) {
    id
    title
    content
    createdAt
    POSITIVE
    NEGATIVE
    User {
      login
    }
    userPost {
        mark
    }
    
  }
}`

const Months: React.FC<MonthsProps> = ({ years }) => {
    const { setPosts, setMonth, setYear, year, month, newPosts, user } = useStore();
    const [expanded, setExpanded] = React.useState<string | false>('panel1');

    const months = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
      ];
    


    const [findPostsQuery, { loading, error, data }] = 
    useLazyQuery(user?FIND_POSTS_FOR_LOGGED_QUERY:FIND_POSTS_QUERY, {
        fetchPolicy: 'network-only',
      });
    
    const handleChange =
        (panel: string) => (event: React.SyntheticEvent, newExpanded: boolean) => {
            setExpanded(newExpanded ? panel : false);
        };

    const findPosts = async (year:number,month:number) => {
        setYear(year);
        setMonth(month);
        const variables:{year: number, month: number, uuid?:string} = { year, month: month+1};
        if(user) {
            variables.uuid= user.uuid;
        }
        const result = await findPostsQuery({variables });
        setPosts(result.data.posts);
    }

    useEffect(()=>{
        findPosts(year,month);
    },[newPosts])
    return (
        <div>

            {Object.keys(years).reverse().map((key, index) => (
                <Accordion key={key} expanded={expanded === 'panel' + (index + 1)} onChange={handleChange('panel' + (index + 1))}>
                    <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                        <Typography>{key}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <ul className='months-list'>                           
                                {
                                    // @ts-ignore                                   
                                    years[key].map(el => (
                                        <li className={year===parseInt(key) && month===el?'selected-month':''} onClick={()=>{ findPosts(parseInt(key),parseInt(el)) }} key={el}>
                                            {/* { '[' +month + ' ' +el + ']' } */}
                                            {months[el]}
                                            </li>
                                    ))}
                            </ul>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
            ))}
        </div>
    );
}

export default Months;