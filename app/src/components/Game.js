const Game = ({title, platform, reviews})=>{
    return <div>
        <h3>{title}</h3>
        <ul>
            { platform.map(platform=>
                <li>{platform}</li>
            )}
        </ul>
        <ul>
            { reviews.map(review=><li>{review.content}</li>) }
        </ul>
    </div>
}

export default Game;