import { useState } from 'react';
import { useMutation, gql } from "@apollo/client";

import PasswordStrengthBar from 'react-password-strength-bar';

import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';

import useStore from "../store";

const REGISTER_MUTATION = gql`
 mutation AddUser($login: String, $password: String) {
  addUser(login: $login, password: $password) {
    
    ... on User {
      login
      uuid
    }
    ... on Message {
      header
      content
    }
  }
}
`;

export interface SimpleDialogProps {
    open: boolean;
    onClose: () => void;
}

function SimpleDialog(props: SimpleDialogProps) {
    const { onClose, open } = props;
    const { login } = useStore();
    const [loginText, setLoginText] = useState<string>('');
    const [passwordText, setPasswordText] = useState<string>('');
    const [repeatPasswordText, setRepeatPasswordText] = useState<string>('');
    const [score, setScore] = useState<number>(0);
    const [messages, setMessages] = useState<string[]>([])
    const [success, setSuccess] = useState<boolean>(false);
    const [registerMutation, { loading, error, data }] = useMutation(REGISTER_MUTATION);

    const onRegister = async () => {
        setMessages([]);
        const newMessages = [];
        if (score < 3) {
            newMessages.push('Password too weak');
//            setMessages(messages => ['Password too weak']);
        }
        if (passwordText !== repeatPasswordText) {
            newMessages.push('Passwords do not match');
            // setMessages(messages => [...messages, 'Passwords do not match']);
        }
        if (newMessages.length > 0) {
            setMessages(newMessages);
            return;
        }
        
        try {
            await registerMutation({ variables: { login: loginText, password: passwordText } });
            setSuccess(true);
            setTimeout(onClose, 3000);

        } catch (e) { 
            if (e instanceof Error && e.message.includes('Unique constraint failed on the constraint: `User_login_key`')) {
                setMessages(messages=>[...messages,'User already exists'])
            }
        }

    };

    return (
        <Dialog onClose={onClose} open={open}>
            <DialogTitle>Register</DialogTitle>
            <Box

                component="form"
                sx={{
                    '& > :not(style)': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="off"
                style={{ display: 'flex', flexDirection: 'column', padding: '20px' }}
            >
                <TextField onChange={(e) => { setLoginText(e.target.value) }} id="standard-basic" label="login" variant="standard" />
                <TextField
                    onChange={(e) => { setPasswordText(e.target.value) }}
                    id="outlined-password-input"
                    variant="standard"
                    label="password"
                    type="password"
                    autoComplete="current-password"
                />
                <TextField
                    onChange={(e) => { setRepeatPasswordText(e.target.value) }}
                    id="outlined-password-input"
                    variant="standard"
                    label="repeat password"
                    type="password"
                    autoComplete="current-password"
                />
            </Box>
            <Stack sx={{ width: '100%', color: 'grey.500' }} spacing={2}>
                <PasswordStrengthBar password={passwordText} scoreWords={['very weak', 'weak', 'too weak', 'good', 'strong']} onChangeScore={(score) => { setScore(score) }} />
            </Stack>
            <ButtonGroup style={{ display: 'flex', justifyContent: 'space-around' }} variant="text" aria-label="Basic button group">
                <Button onClick={onRegister} style={{ margin: '10px' }} color="success">Register</Button>
                <Button onClick={onClose} style={{ margin: '10px' }} color="secondary">Close</Button>
            </ButtonGroup>
            <Stack sx={{ width: '100%' }} spacing={2}>
                {messages.map(message => <Alert severity="error">{message}</Alert>)}
                { success && <Alert severity="success">You have been registered</Alert> }
            </Stack>
        </Dialog>
    );
}

export default () => {
    const { registerDialog, toggleRegisterDialog } = useStore();



    return (
        <div>
            <SimpleDialog
                open={registerDialog}
                onClose={toggleRegisterDialog}

            />
        </div>
    );
}
