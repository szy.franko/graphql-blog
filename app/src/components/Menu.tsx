import React, { useEffect, useState, MouseEvent } from 'react';
import { useLazyQuery, gql } from "@apollo/client";
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import LoginIcon from '@mui/icons-material/Login';
import Logout from '@mui/icons-material/Logout';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { Link, useLocation } from "react-router-dom";

import useStore from "../store";

const LOGOUT_QUERY = gql`
 query Logout($uuid: String) {
  logout(uuid: $uuid) {
    header
    content
  } 
}
`;

const linkCss: React.CSSProperties = {
    color: 'rgba(30, 30, 30, 0.50)',
    textDecoration: 'none',
}
const linkSelectedCss: React.CSSProperties = {
    color: 'rgba(0, 0, 0, 1)',
    textDecoration: 'underline',
}
export default () => {
    const { user, toggleLoginDialog, toggleRegisterDialog, logout } = useStore();

    const [logoutQuery, { loading, error, data }] = useLazyQuery(LOGOUT_QUERY);


    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        logoutQuery({ variables: { uuid: user?.uuid } })
        logout()
    }

    const location = useLocation();
   
    return (
        <React.Fragment>
            <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
                <Typography sx={{ minWidth: 100 }}>
                    <Link to="/" style={location.pathname==='/'?linkSelectedCss:linkCss}>
                        Home
                    </Link>
                </Typography>
                <Typography sx={{ minWidth: 100 }}>
                    <Link to="/blog" style={location.pathname==='/blog'?linkSelectedCss:linkCss}>
                        Blog
                    </Link>
                </Typography>
                <Typography sx={{ minWidth: 100 }}>
                    <Link to="/about" style={location.pathname==='/about'?linkSelectedCss:linkCss}>
                        About
                    </Link>
                </Typography>
                <Tooltip title={user == null ? 'Jump into' : 'Account settings'}>
                    <IconButton
                        onClick={handleClick}
                        size="small"
                        sx={{ ml: 2 }}
                        aria-controls={open ? 'account-menu' : undefined}
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                    >
                        <Avatar sx={{ width: 32, height: 32 }}>M</Avatar>
                    </IconButton>
                </Tooltip>
            </Box>
            <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        mt: 1.5,
                        '& .MuiAvatar-root': {
                            width: 32,
                            height: 32,
                            ml: -0.5,
                            mr: 1,
                        },
                        '&::before': {
                            content: '""',
                            display: 'block',
                            position: 'absolute',
                            top: 0,
                            right: 14,
                            width: 10,
                            height: 10,
                            bgcolor: 'background.paper',
                            transform: 'translateY(-50%) rotate(45deg)',
                            zIndex: 0,
                        },
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >


                {user == undefined && <MenuItem onClick={() => { handleClose(); toggleLoginDialog() }}>
                    <LoginIcon />&nbsp;&nbsp; Login
                </MenuItem>}
                <Divider />

                {user == undefined &&
                    <MenuItem onClick={() => { handleClose(); toggleRegisterDialog() }}>
                        <ListItemIcon>
                            <AccountCircleIcon fontSize="small" />
                        </ListItemIcon>
                        Register
                    </MenuItem>
                }


                {user != undefined && <MenuItem onClick={handleClose}>
                    <Avatar /> Profile
                </MenuItem>}
                <Divider />

                {user != undefined &&
                    <MenuItem onClick={() => { handleLogout(); handleClose() }}>
                        <ListItemIcon>
                            <Logout fontSize="small" />
                        </ListItemIcon>
                        Logout
                    </MenuItem>
                }


            </Menu>
        </React.Fragment>
    );
}