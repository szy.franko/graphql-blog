import { FC, useState, useEffect,useCallback } from "react"
import { useMutation, useLazyQuery, gql } from "@apollo/client";


import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Button from '@mui/material/Button';
import TextField from "@mui/material/TextField";
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import useStore from '../store';

import { Mark } from '../helpers';
import Comment from "./Comment";

const SENDCOMMENT_MUTATION = gql`
  mutation AddComment($content: String, $postId: ID $uuid: String) {
    addComment(content: $content, postId: $postId, uuid: $uuid) {
      ... on Comment {
        id
        createdAt
        content
      }
      ... on Message {
        header
        content
      }
    }
  }
`;

const FIND_COMMENTS_TO_POST_QUERY = gql`
query findCommentsToPost($id:ID!, $uuid: String) {
  findCommentsToPost(id: $id, uuid: $uuid) {
    content
    createdAt
    id
    POSIIVE
    NEGATIVE
    userComment {
        mark
    }
    user {
      login
    }
  }
}`

const SENDUSERPOST_MUTATION = gql`
  mutation AddUserPost($postId: ID, $mark: Mark, $uuid: String) {
    addUserPost(postId: $postId, mark: $mark, uuid: $uuid) {
      ... on UserPost {
        id
       postId
       mark
      }
      ... on Message {
        header
        content
      }
    }
  }
`;

interface Props {
    id: number;
    title: string;
    content: string;
    createdAt: string;
    author: string;
    mark?: Mark;
    POSITIVE: number;
    NEGATIVE: number;
};

interface User {
    login: string
};

interface Comment {
    content: String;
    createdAt: string;
    id: number;
    user: User;
    userComment?: Array<{mark: Mark}>;
    POSIIVE: number;
    NEGATIVE: number;
};

const Post: FC<Props> = ({ id, title, content, createdAt, author, mark, POSITIVE, NEGATIVE }) => {
    const { user, markPost, setNewPosts, newComments, posts } = useStore();
    const [visible, setVisible] = useState<boolean>(false);
    const [commentContent, setCommentContent] = useState<string>('');
    const [comments, setComments] = useState<Comment[]>([]);
    const [postMark, setPostMark] = useState<Mark|undefined>(mark);
    
    const [sendCommentMutation, { loading, error, data }] =
        useMutation(SENDCOMMENT_MUTATION);
    const [sendUserPostMutation] =
        useMutation(SENDUSERPOST_MUTATION);
    const sendComment = useCallback(async () => {
        const result = await sendCommentMutation({
            variables: {
                content: commentContent,
                uuid: user?.uuid,
                toPost: true,
                postId: id
            },
        });
        const resultComments = await findCommentsToPost({ variables: { id, uuid: user?.uuid } });
        setComments(resultComments.data.findCommentsToPost);
        setCommentContent('');
    }, [commentContent])

    const sendUserPost = async (mark: Mark) => {
        const result = await sendUserPostMutation({
            variables: {
                postId: id, mark, uuid: user?.uuid

            }
        })

        
        markPost(id.toString(), mark);
        setNewPosts();
        

    }
    const [findCommentsToPost] = useLazyQuery(FIND_COMMENTS_TO_POST_QUERY, { fetchPolicy: 'network-only', });
    
    const getComments=async ()=>{
        const result = await findCommentsToPost({ variables: { id, uuid: user?.uuid } });
        setComments(result.data.findCommentsToPost);
    }
    const showHideDetails = async () => {
        setVisible(visible => !visible);

        if (!visible) {
            getComments();
        }
    }


    useEffect(()=>{
        (async ()=>{
            const result = await findCommentsToPost({ variables: { id, uuid: user?.uuid } });
            setComments(result.data.findCommentsToPost);
        })();

    }, [newComments]);

    useEffect(() =>{
        setPostMark(mark);
    },[posts]);

    const setUp = () => {
        
    }

    const setDown = () => {

    }

    return <CardContent style={{ border: '1px solid #aaa', margin: '10px', width: '90%' }}>

        <Typography variant="h5" component="div">{title}</Typography>
        {visible &&
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                <div className="post-content"
                    dangerouslySetInnerHTML={{ __html: content }} />
            </Typography>
        }

        <p>Author: {author}, Published: {(new Date(parseInt(createdAt))).toLocaleDateString()} </p>
        {user && visible && <>
            <ThumbUpIcon className={postMark===Mark.POSIIVE?'positive-mark':''} onClick={() => {sendUserPost(Mark.POSIIVE) }} />
            <ThumbDownIcon className={postMark===Mark.NEGATIVE?'negative-mark':''} onClick={() => { sendUserPost(Mark.NEGATIVE) }} />
        </>
        }
        <br />
        <div className="marks-number">
        <p className="positive-mark">{ POSITIVE}</p> <p className="negative-mark">{ NEGATIVE }</p>
        </div>

        {visible && <Box sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
            <List className="comments-list">
                <p style={{ width: 'fit-content' }}>Comments:</p>
                {comments.map((el, key) => <ListItem key={key} disablePadding>
                    {<Comment 
                            id={el.id}
                            content={el.content}
                            createdAt={el.createdAt}
                            login={el.user.login}
                            mark={el.userComment && el.userComment[0] ? el.userComment[0].mark : undefined}
                            POSIIVE={el.POSIIVE}
                            NEGATIVE={el.NEGATIVE}
                            callback={getComments}
                            />}
                </ListItem>)}

            </List>
        </Box>}

        {user !== null && visible && <div className="add-comment-wrapper">
            <TextField
                onChange={(e) => {
                    setCommentContent(e.target.value);
                }}
                value={commentContent}
                multiline
                id="outlined-basic"
                label="Comment"
                variant="outlined"
            />
            <Button onClick={sendComment}>Add comment</Button>
        </div>
        }

        <Button onClick={showHideDetails}>
            {visible && <p>hide</p>}
            {!visible && <p>show</p>}
        </Button>
    </CardContent>
}

export default Post;