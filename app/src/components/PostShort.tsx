import { FC, useState } from "react"
import Button from '@mui/material/Button';
interface Props {
    title: string;
    content: string;
    createdAt: string;
    author: string;
}
const Post: FC<Props> = ({ title, content, createdAt, author }) => {
    const [visible, setVisible] = useState<boolean>(false);
    return <div className="post-wrapper">
    <div className="post">
        <h3 className="post-title">{title}</h3>
        { visible && <div dangerouslySetInnerHTML={{ __html: content }} /> }
        <p>Author: {author}, Published: {(new Date(parseInt(createdAt))).toLocaleDateString()} </p>
    </div>
    <Button onClick={()=>setVisible(visible=>!visible)}>Show/hide</Button>
    </div>
}

export default Post;