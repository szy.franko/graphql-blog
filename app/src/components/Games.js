import { useQuery, gql } from "@apollo/client";
import Game from "./Game";

const GAMES_QUERY = gql`
  query Games {
    games {
      platform
      reviews {
        content
      }
      title
    }
  }
`;
const Games = () => {
  const { data } = useQuery(GAMES_QUERY);

  if(typeof data==='undefined') return <div>Fetching data</div>
  return <div>

        {data.games.map(game=>
             <Game title={game.title} platform={game.platform}
             reviews={game.reviews}/>
        )}
  </div>
 ;
};

export default Games;
